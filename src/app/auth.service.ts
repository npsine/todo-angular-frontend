import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable()
export class AuthService {
    token: string;

    constructor(
        private router: Router,
        private httpClient: HttpClient
    ) {
    }

    setToken(token: string) {
        localStorage.setItem('access-token', token);
    }

    getToken() {
        return localStorage.getItem('access-token');
    }

    setUser(user: object) {
        localStorage.setItem('user', JSON.stringify(user));
    }

    getUser() {
        return localStorage.getItem('user');
    }

    isLoggedIn() {
        return this.getToken() !== null;
    }

    login(login: object) {
        return this.httpClient.post(`${environment.serverUrl}/user/login`, login);
    }

    logout() {
        localStorage.removeItem('access-token');
        this.router.navigate(['login']);
    }

    forgotPassword(email: string) {
        return this.httpClient.post(`${environment.serverUrl}/user/forgot`, {email});
    }

    createUser(register: object) {
        return this.httpClient.post(`${environment.serverUrl}/user`, register);
    }

    loadUser() {
        return this.getUser();
    }

    resetPassword(data) {
        console.log(data)
        return this.httpClient.post(`${environment.serverUrl}/user/reset`, data);
    }
}
