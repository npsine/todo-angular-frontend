import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ForgotCompleteComponent} from './forgot-complete.component';

describe('ForgotCompleteComponent', () => {
    let component: ForgotCompleteComponent;
    let fixture: ComponentFixture<ForgotCompleteComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ForgotCompleteComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ForgotCompleteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
