import {Injectable} from '@angular/core';
import {Todo} from '../todo/todo';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../auth.service';

@Injectable()
export class TodoService {

    constructor(
        private httpClient: HttpClient,
        private authService: AuthService
    ) {
    }

    getAllTodos() {
        return this.httpClient.get(`${environment.serverUrl}/todo`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.authService.getToken()}`
            }
        });
    }

    getTodoById(id) {
        return this.httpClient.get(`${environment.serverUrl}/todo/${id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.authService.getToken()}`
            }
        });
    }

    addNewTodo(data) {
        return this.httpClient.post(`${environment.serverUrl}/todo`, data, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.authService.getToken()}`
            }
        });
    }

    editTodo(id, data) {
        return this.httpClient.post(`${environment.serverUrl}/todo/${id}`, {todo: data}, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.authService.getToken()}`
            }
        });
    }

    finishTodo(id, data) {
        return this.httpClient.post(`${environment.serverUrl}/todo/finish/${id}`, data, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.authService.getToken()}`
            }
        });
    }

    deleteTodo(id) {
        return this.httpClient.post(`${environment.serverUrl}/todo/${id}/delete`, null, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.authService.getToken()}`
            }
        });
    }

}
