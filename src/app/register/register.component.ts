import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    email: string;
    password: string;
    fullname: string;
    isRequired: boolean | false;
    isInvalidPattern: boolean | false;
    isDuplicate: boolean | false;
    reg = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

    constructor(private authService: AuthService, private router: Router) {
    }

    ngOnInit(): void {
    }

    onChange() {
        this.isRequired = false;
        this.isInvalidPattern = false;
        this.isDuplicate = false;
    }

    isValid(email) {
        return this.reg.test(String(email).toLowerCase());
    }

    handleRegister() {
        if (this.email && this.password && this.fullname) {
            const isValid = this.isValid(this.email);
            if (isValid) {
                this.authService.createUser({email: this.email, password: this.password, fullname: this.fullname})
                    .subscribe((response: any) => {
                        if (response.success === true) {
                            this.authService.setToken(response.response.token);
                            this.authService.setUser(response.response.user);
                            this.router.navigate(['register-complete']);
                        } else {
                            this.isDuplicate = true;
                        }
                    });
            } else {
                this.isInvalidPattern = true;
            }
        } else {
            this.isRequired = true;
        }
    }
}
