import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-reset',
    templateUrl: './reset.component.html',
    styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

    password: string
    token: string
    confirmPassword: string
    isRequired: boolean | false
    isErrors: boolean | false
    isMissMatch: boolean | false

    constructor(
        private authService: AuthService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
    }

    onChange() {
        this.isRequired = false
        this.isMissMatch = false
        this.isErrors = false
    }

    handleConfirm() {
        if (this.password && this.confirmPassword) {
            if (this.password === this.confirmPassword) {
                this.activatedRoute.queryParams.subscribe(params => {
                    this.token = params['token'];
                })
                this.authService.resetPassword({password: this.password, token: this.token.replace(/ /g, '+')})
                    .subscribe((response: any) => {
                        if (response.success === true) {
                            this.router.navigate(['reset-complete']);
                        } else {
                            this.isErrors = true;
                        }
                    });
            } else {
                this.isMissMatch = true;
            }
        } else {
            this.isRequired = true;
        }
    }

}
