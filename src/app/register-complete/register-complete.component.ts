import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {User} from '../todo/user';

@Component({
    selector: 'app-register-complete',
    templateUrl: './register-complete.component.html',
    styleUrls: ['./register-complete.component.css']
})
export class RegisterCompleteComponent implements OnInit {
    user: User;

    constructor(private authService: AuthService) {
    }

    ngOnInit(): void {
        this.loadUser();
    }

    loadUser() {
        const obj = this.authService.loadUser();
        const data = JSON.parse(obj);
        this.user = {
            fullname: data.fullname,
            id: data.id,
            email: data.email
        };
    }
}
