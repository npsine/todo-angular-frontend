import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-forgot',
    templateUrl: './forgot.component.html',
    styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
    email: string;
    isRequired: boolean | false;
    isInvalidPattern: boolean | false;
    isNotFound: boolean | false;
    reg = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

    constructor(private authService: AuthService, private router: Router) {
    }

    ngOnInit(): void {
    }

    onChange() {
        this.isRequired = false;
        this.isInvalidPattern = false;
        this.isNotFound = false;
    }

    isValid(email) {
        return this.reg.test(String(email).toLowerCase());
    }

    handleForgot() {
        if (this.email) {
            const isValid = this.isValid(this.email);
            if (isValid) {
                this.authService.forgotPassword(this.email)
                    .subscribe((response: any) => {
                        if (response.success === true) {
                            this.router.navigate(['forgot-complete']);
                        } else {
                            this.isNotFound = true;
                        }
                    });
            } else {
                this.isInvalidPattern = true;
            }
        } else {
            this.isRequired = true;
        }
    }
}
