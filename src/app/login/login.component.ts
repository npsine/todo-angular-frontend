import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    email: string | '';
    password: string | '';
    isRequired: boolean | false;
    isInvalidPattern: boolean | false;
    isNoEmail: boolean | false;
    reg = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

    constructor(private authService: AuthService, private router: Router) {
    }

    ngOnInit(): void {
    }

    onChange() {
        this.isRequired = false;
        this.isInvalidPattern = false;
        this.isNoEmail = false;
    }

    isValid(email) {
        return this.reg.test(String(email).toLowerCase());
    }

    handleLogin() {
        if (this.email && this.password) {
            const isValid = this.isValid(this.email);
            if (isValid) {
                this.authService.login({email: this.email, password: this.password})
                    .subscribe((response: any) => {
                        if (response.success === true) {
                            this.authService.setToken(response.response.token);
                            this.authService.setUser(response.response.user);
                            this.router.navigate(['todo']);
                        } else {
                            this.isNoEmail = true;
                        }
                    });
            } else {
                this.isInvalidPattern = true;
            }
        } else {
            this.isRequired = true;
        }
    }
}
