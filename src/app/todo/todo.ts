export class Todo {
    id: number;
    todo = '';
    isFinished = false;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
