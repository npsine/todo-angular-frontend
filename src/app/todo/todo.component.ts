import {Component, OnInit} from '@angular/core';
import {Todo} from './todo';
import * as moment from 'moment';
import {TodoService} from '../services/todo.service';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {User} from './user';
import {ModalService} from '../modal.service';

@Component({
    selector: 'app-todo',
    templateUrl: './todo.component.html',
    styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
    public todos: Todo[] = []
    public user: User
    today: string
    task: string
    isRequired: boolean | false
    editId: number

    constructor(
        private router: Router,
        private todoService: TodoService,
        private authService: AuthService,
        private modalService: ModalService
    ) {
        this.today = moment().format('MMMM DD, YYYY');
    }

    ngOnInit() {
        this.loadUser();
    }

    loadUser() {
        const obj = this.authService.loadUser();
        const data = JSON.parse(obj);
        this.user = {
            fullname: data.fullname,
            id: data.id,
            email: data.email
        };
        this.loadAllTodoList();
    }

    handleLogout() {
        this.authService.logout();
    }

    loadAllTodoList() {
        this.todoService.getAllTodos()
            .subscribe((response: any) => {
                this.todos = response.response.todo
            })
    }

    onClickAddTodo() {
        this.openModal('add-modal');
    }

    onClickEditTodo(id) {
        this.todoService.getTodoById(id)
            .subscribe((response: any) => {
                this.task = response.response.todo.todo
                this.editId = id
                this.openModal('edit-modal');
            })
    }

    onFinishTodo(id, flag) {
        const data = {
            isFinished: flag,
        }
        this.todoService.finishTodo(id, data)
            .subscribe((response: any) => {
                this.loadAllTodoList();
            });
    }

    onClickDeleteTodo(id) {
        this.todoService.deleteTodo(id)
            .subscribe((response: any) => {
                this.loadAllTodoList();
            });
    }

    openModal(id: string) {
        this.modalService.open(id);
    }

    closeModal(id: string) {
        this.modalService.close(id);
    }

    addNewTask() {
        if (this.task) {
            this.todoService.addNewTodo({todo: this.task})
                .subscribe((response: any) => {
                    this.closeModal('add-modal');
                    this.loadAllTodoList();
                    this.task = ''
                })
        } else {
            this.isRequired = true
        }
    }

    editTask() {
        if (this.task) {
            this.todoService.editTodo(this.editId, this.task)
                .subscribe((response: any) => {
                    this.editId = 0
                    this.task = ''
                    this.closeModal('edit-modal');
                    this.loadAllTodoList();
                })
        } else {
            this.isRequired = true
        }
    }
}
