export class User {
    id: any
    fullname: string;
    email: string

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
