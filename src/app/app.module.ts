import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {ForgotComponent} from './forgot/forgot.component';
import {TodoComponent} from './todo/todo.component';
import {TodoService} from './services/todo.service';
import {AuthGuard} from './auth.guard';
import {AuthService} from './auth.service';
import {HttpClientModule} from '@angular/common/http';
import {ForgotCompleteComponent} from './forgot-complete/forgot-complete.component';
import {RegisterCompleteComponent} from './register-complete/register-complete.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ModalComponent} from './modal/modal.component';
import {ResetComponent} from './reset/reset.component';
import {ResetCompleteComponent} from './reset-complete/reset-complete.component';

const routes: Routes = [
    {path: 'todo', component: TodoComponent, canActivate: [AuthGuard]},
    {path: 'reset-complete', component: ResetCompleteComponent},
    {path: 'reset', component: ResetComponent},
    {path: 'forgot-complete', component: ForgotCompleteComponent},
    {path: 'forgot', component: ForgotComponent},
    {path: 'register-complete', component: RegisterCompleteComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'login', component: LoginComponent},
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: '**', redirectTo: 'login', pathMatch: 'full'},
];

@NgModule({
    declarations: [
        AppComponent,
        RegisterComponent,
        LoginComponent,
        ForgotComponent,
        TodoComponent,
        ForgotCompleteComponent,
        RegisterCompleteComponent,
        ModalComponent,
        ResetComponent,
        ResetCompleteComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [RouterModule],
    providers: [
        TodoService,
        AuthService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
